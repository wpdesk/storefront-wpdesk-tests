<?php

add_filter( 'flexible-shipping/group-method/supports/shipping-zones', function ( $status ) {
	$option_value = get_option( 'wpdesk_flexible_shipping_group_method_supports_shipping_zones', '' );
	if ( $option_value ) {
		return true;
	}

	return $status;
} );

add_filter( 'flexible-shipping/group-method/supports/edit', function ( $status ) {
	$option_value = get_option( 'wpdesk_flexible_shipping_group_method_supports_edit', '' );
	if ( $option_value ) {
		return true;
	}

	return $status;
} );
