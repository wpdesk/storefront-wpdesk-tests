<?php

if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && 'https' === $_SERVER['HTTP_X_FORWARDED_PROTO'] ) { $_SERVER['HTTPS'] = 'on'; }

//include __DIR__. "/baselinker_functions.php";

if ( file_exists( __DIR__. '/functions_add.php' ) ) {
	include __DIR__ . '/functions_add.php';
}

include __DIR__ . '/functions_rest_api.php';
include __DIR__ . '/functions_flexible_shipping.php';

/**
 * Disable images regenerations
 */
add_filter( 'woocommerce_background_image_regeneration', '__return_false' );

/**
 * Send tracker data to testdata.wpdesk.org instead of data.wpdesk.org
 */
add_filter( 'wpdesk_tracker_use_testdata', '__return_true' );

add_filter( 'wpdesk_tracker_enabled', '__return_true' );

add_filter( 'flexible_shipping_ups_testing', '__return_true' );

add_filter( 'flexible_shipping_dhl_express_testing', '__return_true' );
