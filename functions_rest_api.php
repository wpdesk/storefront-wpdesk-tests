<?php

add_filter( 'woocommerce_rest_check_permissions', 'storefront_wpdesk_allow_rest_api_queries', 10, 4 );
function storefront_wpdesk_allow_rest_api_queries( $permission, $context, $zero, $object ) {
	$disable_permissions = get_option( 'wpdesk_rest_api_disable_permissions', '' );
	if ( $disable_permissions ) {
		return true;  // Allow all queries.
	}
	return $permission;
}