[2.0.1] - 2024-06-26
# Changed
* align with original WooCommerce template

[2.0.0] - 2021-05-04
# Changed
* moved to folder
  
[1.0.3] - 2019-10-30
# Added
* filter wpdesk_tracker_use_testdata

[1.0.2] - 2019-10-14
# Added
* shipping method woocommerce template

[1.0.1] - 2019-09-06
# Added
* functions.php
* Disable images regenerations filter

[1.0.0] - 2019-08-20
# Added
* First version
